import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TrackController {
  constructor(
    @InjectModel(Track.name)
    private trackModel: Model<TrackDocument>,
  ) {}
  @Get()
  getAll(@Query() album: string) {
    const albumId = Object.values(album);
    let query = {};
    if (albumId.length > 0) {
      query = { album: { _id: albumId[0] } };
    }
    return this.trackModel.find(query).populate('album', 'name');
  }
  @Post()
  create(@Body() trackDto: CreateTrackDto) {
    const track = new this.trackModel({
      name: trackDto.name,
      album: trackDto.album,
      duration: trackDto.duration,
      isPublished: trackDto.isPublished,
    });
    return track.save();
  }
  @Delete(':id')
  removeTrack(@Param('id') id: string) {
    this.trackModel.deleteOne({ _id: id });
    return { message: 'Delete track id = ' + id };
  }
}
