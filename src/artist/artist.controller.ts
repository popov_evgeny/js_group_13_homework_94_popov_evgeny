import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Artist, ArtistDocument } from '../schemas/artist.schema';
import { Model } from 'mongoose';
import { CreateArtistDto } from './create-artist.dto';

@Controller('artists')
export class ArtistController {
  constructor(
    @InjectModel(Artist.name)
    private artistModel: Model<ArtistDocument>,
  ) {}
  @Get()
  getAll() {
    return this.artistModel.find();
  }
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.artistModel.find({ _id: id });
  }
  @Post()
  create(@Body() artistDto: CreateArtistDto) {
    const artist = new this.artistModel({
      name: artistDto.name,
      information: artistDto.information,
      isPublished: artistDto.isPublished,
    });
    return artist.save();
  }
  @Delete(':id')
  async removeArtist(@Param('id') id: string) {
    await this.artistModel.deleteOne({ _id: id });
    return { message: 'Delete artist id = ' + id };
  }
}
