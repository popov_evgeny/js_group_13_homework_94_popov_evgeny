import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { CreateAlbumDto } from './create-album.dto';

@Controller('albums')
export class AlbumController {
  constructor(
    @InjectModel(Album.name)
    private albumModel: Model<AlbumDocument>,
  ) {}
  @Get()
  getAll(@Query() artist: string) {
    const authorId = Object.values(artist);
    let query = {};
    if (authorId.length > 0) {
      query = { author: { _id: authorId[0] } };
    }
    return this.albumModel.find(query).populate('author', 'name');
  }
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.albumModel.find({ _id: id });
  }
  @Post()
  create(@Body() albumDto: CreateAlbumDto) {
    const album = new this.albumModel({
      name: albumDto.name,
      author: albumDto.author,
      year: albumDto.year,
      isPublished: albumDto.isPublished,
    });
    return album.save();
  }
  @Delete(':id')
  removeAlbum(@Param('id') id: string) {
    this.albumModel.deleteOne({ _id: id });
    return { message: 'Delete album id =' + id };
  }
}
