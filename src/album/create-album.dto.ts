export class CreateAlbumDto {
  name: string;
  author: string;
  year: string;
  isPublished: boolean;
}
